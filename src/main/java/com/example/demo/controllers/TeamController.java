package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.Team;

@RestController
@RequestMapping("/team")
public class TeamController {
    private List<Team> teams = new ArrayList<>();
    
    @GetMapping("/list")
    public List<Team> getList() {
        return teams;
    }

    @PostMapping("/add")
    public void add(@RequestBody Team team) {
        teams.add(team);
    }
}
